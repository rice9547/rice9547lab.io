---
title: "Flexbox Layout"
date: 2020-03-16T17:49:34+08:00
categories:
 - 
tags:
 - frontend
 - web
 - css
featured_image:
---

應該是第一次發前端文，這篇適合給有寫過最簡單的 html 和 css 的新手觀看。
初次接觸 Flexbox 的佈局方式是寫 React-Native，而 CSS 有支援的關係，現在寫 web 前端也多用 flexbox 做排版。
<!--more-->

#### flexbox 原理
若今天有一個視窗，`display`方式為`flex`，內容有兩個 div，先設定其 `min-height: 100vh` 方便顯示，再給予`flex:1`的值，接著分別設定 `background-color` 為 <span style="color: red;">red</span> 和 <span style="color: blue;">blue</span>，畫面會長這樣:
![](https://i.imgur.com/F1WCpvi.png)
如果將 `flex:1` 的部分其中一個改為 `flex:2`(這邊我改紅色的)，則畫面中紅色的元素變多了:
![](https://i.imgur.com/tTl4YGp.png)
其實 flexbox 的想法就是依照比例分割，前面我們先宣告一個視窗要使用 flexbox 排版，接著裡僅有的兩個元素我們說他們佔有的分別是 2 和 1，則兩個元素會以 2:1 的比例顯示。建議可以直接拉看看網頁大小，會發現不論視窗多大，他都會依照 2:1 去顯示。

如果你寫過網頁，應該會發現一件奇怪的事情: 裡面的元素是橫向的，原因是 flexbox 預設的元素編排方式就是橫向的，而非一般網頁設計中的直向，這也是為什麼有些網頁上，要將元素橫向排列時，有人會直接在上層元素的 css 加上 `display: flex` 的原因。

若需要更改，可以在最上層(設定顯示方式的那層)設定`flex-direction`為其他的屬性，例如`column`。至於 `row-reverse` 和 `column-reverse` 常被說無用，實際上用途不少，最簡單的例子是部落格文章正序/反序顯示，只要把 css 在 reverse 和沒有 reverse 的版本中切換就好。

#### flexbox 排版

到這裡我們認識了最簡單的排版方式，如果只能做到這樣的事情，那也沒有什麼好說的，所以我們來讓情況變複雜。

假設有 5 個元素，我們固定其寬高為 `50px`，父層元素寬高為 `300px`:

![](https://i.imgur.com/Fz9LMDX.png)

我們希望他們的位子是在整個視窗的正中間，如:

![](https://i.imgur.com/EXUoTlF.png)

可以使用以下兩個 css:

```css=
justify-content: center;
align-items: center;
```
若只寫 `justify-content: center`，會發現內部元素只有水平置中；若只寫 `align-items: center`，則內部元素只有垂直置中。由此可知，`justify-content`影響的是「跟 flex 方向同方向的那個方向」，例如這裡的方向是預設的橫向，就只會讓他水平置中；而`align-items` 影響的是另一個方向。

而屬性內容也很多樣，兩者則有些差別，例如 `justify-content` 有以下內容:

- `center`
    - 置中
- `flex-start`
    - 跟著上層元素的開始。
    - ![](https://i.imgur.com/QHwFIUP.png)
- `flex-end`
    - 跟著上層元素結束的地方。
    - ![](https://i.imgur.com/3Ph4g1X.png)
- `space-around`
    - 每個元素彼此有相同間距，和上層元素的間距為彼此間距的一半。
    - ![](https://i.imgur.com/ZiCEafY.png)
- `space-between`
    - 每個元素彼此有相同間距，和上層元素無間距。
    - ![](https://i.imgur.com/8sACy1T.png)
- `space-evenly`
    - 每個元素彼此有相同間距，和上層元素的間距同彼此的間距。
    - ![](https://i.imgur.com/5Y4Kqhq.png)

`align-items` 保留很好用的 `center`, `flex-start`, `flex-end`，多了以下的內容:

- stretch
    - 不設定長寬時，自動延伸到跟上層元素一樣大
    - 用途...等需要顯示圖片時，應該會很明顯XD
- base-line
    - 有時候元素可能不等高，這裡就是讓他們依照中線對齊

這裡提到的 `jusity-content` 和 `align-items` 都是一個元素對於底下的元素做排版，那有沒有辦法對自己排版呢？基本上可以使用 `align-self`，屬性內容同 `align-items`，如果上層元素有設定 `align-items`，會優先套用自己的 `align-self`。

#### 其他
flexbox 的排版教學大概到這邊結束，比較值得額外提的是截圖的部分，在 chrome 上對一個元素按滑鼠右鍵->檢查後，可以按下 `ctrl`/`command` + `shift` + `P`，輸入 `node`，會有以下指令可用:

![](https://i.imgur.com/evDh5Xl.png)

第一個就是擷取這個元素了，會幫你存成圖片，本文看到的那些圖片幾乎都是這樣來的。