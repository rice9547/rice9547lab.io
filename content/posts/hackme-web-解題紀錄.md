---
title: hackme web 解題紀錄
date: 2019-05-11 02:18:03
tags:
- CTF
- Web
---
其實很久前就知道 CTF，嘗試解過一題 SQL Injection 後就繼續打算法競賽，但最近突然又想玩一下，就找了新手友善(應該吧)的 [hackme](https://hackme.inndy.tw) 來嘗試。

這篇會持續更新，由於對 web 比較熟悉，所以打算先解 web。以我目前接觸的領域來說，下一個有可能的應該是 Misc 和 Crypto。
<!--more-->

#### hide and seek
這是我的第一題 web，連結連到的是首頁，看到提示說離我很近，感覺就是 source code，所以檢視原始碼，ctrl + f 找 flag，就看到赤裸裸的 flag 躺在那邊等我輸入了XD

#### LFI
由於題目的說明說是 LFI，觀察路徑後感覺是類似這樣的 php code:

```php
include($_GET['page']);
```
先觀察一下原始碼，發現有一個 pages/flag.php 的檔案，感覺就很有戲。

在網頁中可以 GET `php://` 執行一些指令，例如 input 到 command 之類的，而我們要看檔案，可以使用 `php://filter/` 來做。

但直接把檔案印在網頁上的話，又會只看到 body 而看不到 php code，這時候可以考慮把那個檔案轉成 base64 顯示，純文字總不會阻止我了吧(? 使用方式為 `php://filter/read=convert.base64-encode/resource=` ，把 resource 指定 pages/flag 即可。

> Q2FuIHlvdSByZWFkIHRoZSBmbGFnPD9waHAgcmVxdWlyZSgnY29uZmlnLnBocCcpOyA/Pj8K

得到這串 base64 後去 decode，發現原始碼為

```php
Can you read the flag<?php require('config.php'); ?>?
```
對 config.php 做差不多的事情，就噴出 FLAG 啦～

#### homepage
點進去發現又回到首頁了，因為提示叫我看 source code，所以先檢視原始碼。

看著看著，除了 <strong>hide and seek </strong> 的 flag 外，似乎沒其他東西了，想了一下，決定把所有連結都點過一輪，不小心戳出 [cute.js](https://hackme.inndy.tw/cute.js)，看到滿滿的顏文字整個傻掉。

Google 一下後發現是 aadecode，<del>因為很有趣，所以順便看了一些代碼混淆的東西</del>，總之把 code 拿去做 decode 後，看到是一段印 QR Code 的 code，直接用瀏覽器內建的 console 執行，就真的噴出 QR Code 了。

這時候發現因為圖片太大，間隔問題似乎我的掃描器掃不到，所以截圖下來縮小再掃，FLAG GET！

#### scoreboard
這題是意外的驚喜，在解 **homepage** 的時候有打開開發者工具，因為上次開是為了寫爬蟲，所以停在 network 那邊，不小心按到 scoreboard 後，發現 header 有個 **x-flag**，送出後就發現解完了@@

若單純是看這題的話，我應該會等很久才發現這件事，因為沒想過會塞 header。

#### login as admin 0
這題提示說是 SQL Injection，還有 source code 能看，於是先嘗試個

```
admin = 'or 1=1#
password = OAO
```
發現完全沒做登入這件事，檢查一下 source code，發現 `or 1=1` 直接被過濾掉，於是改用 2=2。
也發現 `'` 會直接被取代成`\'`，於是使用`\\'`讓它挑脫斜線而不是引號。

現在我們嘗試這組：

```
admin = \' or 2=2#
password = OAO
```
發現居然是以 guest 登入的，資料庫第一筆不是 admin！想了一下，因為 admin 字典序蠻前面的，加上 source code 有給 schema，就直接對 user 做排序吧！

```
admin = \' or 2=2 order by user#
password = OAO
```
這次就真的拿到 flag 了！不得不說，hackme 的 flag 其實蠻有梗的...。