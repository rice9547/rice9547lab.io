---
title: "Pip3 Install Import Error"
date: 2019-12-30T00:28:29+08:00
draft: true
categories:
 - 
tags:
 - Python
 - Ubuntu
featured_image:
---
今天升級了一下 pip3 的版本

`pip3 install --upgrade pip`

發現升級完畢後 pip3 install 的指令會壞掉

```shell
Traceback (most recent call last):
  File "/usr/bin/pip3", line 9, in <module>
    from pip import main
ImportError: cannot import name 'main'
```
原因是更新後，library 裡面的 function 有改過，但 pip3 卻沒有去更新呼叫方式，於是我們直接找到 pip3 的檔案去修改

`emacs -nw /usr/bin/pip3`

import 的 main 改成 __main__

呼叫的 main() 改成 __main__._main()

這篇就當記錄一下，我猜應該也有人還沒更新，之後會遇到這個問題的XD

(好像是第一次打這麼短的文章)